﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioConHerencia
{
    public class Empleado: Persona
    {
        public int AnioEntrada { get; set; }
        public int NumeroDespacho { get; set; }

        public Empleado()
        {
        }
        public void AsignarAnioEntrada(int AnioAsignado)
        {
            this.AnioEntrada = AnioAsignado;
        }
        public void ReasignarDespacho(int NuevoDespacho)
        {
            this.NumeroDespacho = NuevoDespacho;
        }

        public string MostarDatos()
        {
            return "Año de entrada: "+this.AnioEntrada + " \nNº despacho: " + this.NumeroDespacho;
        }
    }
}
