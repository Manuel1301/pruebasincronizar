﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioConHerencia
{
    public class Persona
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string CedulaIdentidad { get; set; }
        public string EstadoCivil { get; set; }

        public Persona()
        {

        }
        public void CambiarEstadoCivil(string NuevoEstadoCivil)
        {
            this.EstadoCivil = NuevoEstadoCivil;
        }
        public string DevolverDatos()
        {
            return "Nombre: "+this.Nombres + " " + this.Apellidos +" \nCI: " + this.CedulaIdentidad + " \nEstado civil: " + this.EstadoCivil;
        }
    }
}
