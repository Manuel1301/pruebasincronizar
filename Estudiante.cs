﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioConHerencia
{
    public class Estudiante : Persona
    {
        public int Nivel { get; set; }
        public Estudiante()
        {
        }
        public void MatricularNivel(int NuevoNivel)
        {
            this.Nivel = NuevoNivel;
        }
        public string MostrarDatosEstudiante()
        {
            return "Nivel actual matriculado: "+this.Nivel;
        }
        //public void Nivel(int AsignarNivel)
        //{
   
        //}
    }
}