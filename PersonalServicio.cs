﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioConHerencia
{
    class PersonalServicio: Empleado
    {
        public string Seccion { get; set; }
        public PersonalServicio()
        {
             
        }
        public void CambioSeccion(string NuevaSeccion)
        {
            this.Seccion = NuevaSeccion;
        }
        public string MostrarDatoPerServ()
        {
            return "Sección: "+this.Seccion;
        }
    }
}
