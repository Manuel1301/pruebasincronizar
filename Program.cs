﻿using System;

namespace EjercicioConHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //                                  PERSONA( NOMBRE, APELLIDO, CI, ESTADO CIVIL)
            //      EMPLEADO( AÑO DE ENTRADA, NUMERO DE DESPACHO)                 ESTUDIANTE(SEMESTRE)
            //PROFESOR(DEPARTAMENTO)       PERSONAL SERVICIO(SECCION)


            Profesor profesor = new Profesor();//-->Instancia de clase
            Console.WriteLine("PROFESOR");
            profesor.Nombres="Edgardo";
            profesor.Apellidos = "Panchana";
            profesor.CedulaIdentidad = "2555268558";
            profesor.CambiarEstadoCivil("Soltero");
            profesor.CambioDepartamento("POO");
            profesor.AsignarAnioEntrada(2015);
            profesor.ReasignarDespacho(5);

            Console.WriteLine(profesor.DevolverDatos());
            Console.WriteLine(profesor.MostrarDatoProf());
            Console.WriteLine(profesor.MostarDatos());
            Console.WriteLine("");
            Console.WriteLine("");



            Estudiante estudiante = new Estudiante();
            Console.WriteLine("ESTUDIANTE");
            estudiante.Nombres = "Manuel";
            estudiante.Apellidos = "Zambrano";
            estudiante.CedulaIdentidad = "1317188025";
            estudiante.EstadoCivil = "Soltero";
            estudiante.MatricularNivel(3);

            Console.WriteLine(estudiante.DevolverDatos());
            Console.WriteLine(estudiante.MostrarDatosEstudiante());
            Console.WriteLine("");
            Console.WriteLine("");



            PersonalServicio personalServicio = new PersonalServicio();
            Console.WriteLine("PERSONAL DE SERVICIO");
            personalServicio.Nombres = "Carlos";
            personalServicio.Apellidos = "Andrade";
            personalServicio.CedulaIdentidad = "2581215855";
            personalServicio.EstadoCivil = "Casado";
            personalServicio.CambioSeccion("Biblioteca");
            personalServicio.AsignarAnioEntrada(2013);
            personalServicio.ReasignarDespacho(8);

            Console.WriteLine(personalServicio.DevolverDatos());
            Console.WriteLine(personalServicio.MostrarDatoPerServ());
            Console.WriteLine(personalServicio.MostarDatos());
            Console.WriteLine("");
            Console.WriteLine("");

        }
    }
}
