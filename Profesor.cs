﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioConHerencia
{
    class Profesor: Empleado
    {
        public string Departamento { get; set; }
        public Profesor()
        {

        }
        public void CambioDepartamento(string NuevoDepartamento)
        {
            this.Departamento = NuevoDepartamento;
        }
        public string MostrarDatoProf()
        {
            return "Departamento: " + this.Departamento;
        }
    }
}
